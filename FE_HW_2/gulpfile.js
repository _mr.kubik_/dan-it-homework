const { src, dest, watch, series, parallel } = require("gulp"),
  sass = require("gulp-sass")(require("sass")),
  autoprefixer = require("gulp-autoprefixer"),
  cleanCSS = require("gulp-clean-css"),
  imagemin = require("gulp-imagemin"),
  uglify = require("gulp-uglify"),
  concat = require("gulp-concat"),
  fileinclude = require("gulp-file-include"),
  browser = require("browser-sync").create(),
  del = require("del"),
  sourcemap = require("gulp-sourcemaps");

const files = {
  html: "./index-files/!(_)*.html",
  normalizePath: "src/sass/normilize.scss",
  fontsPath: "src/sass/fonts.scss",
  fontsFilePath: "src/sass/fonts/*",
  scssPath: "src/sass/style.scss",
  rawScssPath: "src/sass/**/*.scss",
  jsPath: "src/js/*.js",
  imagePath: "src/img/*png",
};

function fontsFileTask() {
  return src(files.fontsFilePath).pipe(dest("dist/style/fonts"));
}

function cssTask() {
  return src(files.scssPath)
    .pipe(sourcemap.init())
    .pipe(sass())
    .pipe(autoprefixer("last 2 versions"))
    .pipe(concat("styles.min.css"))
    .pipe(cleanCSS())
    .pipe(sourcemap.write("."))
    .pipe(dest("dist/style/"))
    .pipe(browser.stream());
}

function jsTask() {
  return src(files.jsPath)
    .pipe(concat("scripts.min.js"))
    .pipe(uglify())
    .pipe(dest("dist/js/"))
    .pipe(browser.stream());
}

function imgTask() {
  return src(files.imagePath)
    .pipe(
      imagemin({
        progressive: true,
        svgiPlugins: [{ removeViewBox: false }],
        interlaced: true,
        optimizationLevel: 3,
      })
    )
    .pipe(dest("dist/img"))
    .pipe(browser.stream());
}

function cleanDist() {
  return del("dist");
}

function html() {
  return src(files.html)
    .pipe(fileinclude())
    .pipe(dest("./"))
    .pipe(browser.stream());
}

function watchTask() {
  browser.init({
    server: {
      baseDir: "./",
    },
    port: 3000,
    notify: false,
    tunnel: true,
  });

  watch(
    [files.jsPath, files.rawScssPath, files.imagePath, "./index-files/*.html"],
    parallel(cssTask, jsTask, imgTask, html)
  );
  watch("./index-files/*.html", browser.reload);
}

exports.cssTask = cssTask;
exports.html = html;
exports.jsTask = jsTask;
exports.imgTask = imgTask;

exports.build = series(
  cleanDist,
  parallel(cssTask, jsTask, imgTask, html, fontsFileTask)
);

exports.dev = series(watchTask);
