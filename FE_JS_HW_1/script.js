class Employee {
  constructor(name, age, salary) {
    this._name = name;
    this._age = age;
    this._salary = salary;
  }

  set name(value) {
    this._name = value;
  }

  get name() {
    return this._name;
  }

  set age(value) {
    this._age = value;
  }

  get age() {
    return this._age;
  }

  set salary(value) {
    this._salary = value;
  }

  get salary() {
    return this._salary;
  }
}

class Programmer extends Employee {
  constructor(name, age, salary, lang) {
    super(name, age, salary);
    this.lang = lang;
  }

  set lang(value) {
    this._lang = [...value];
  }

  get lang() {
    return this._lang;
  }

  set salary(value) {
    this._salary = value;
  }

  get salary() {
    return this._salary * 3;
  }
}

const user1 = new Programmer("Lira", 20, 2000, ["Java", "Mava"]);
const user2 = new Programmer("Rina", 20, 5000, ["Java", "Kava"]);
const user3 = new Programmer("Viva", 20, 10000, ["Java", "Mava", "Kava"]);

console.log(user1);
console.log(user2);
console.log(user3);

console.log(user1.salary);
console.log(user2.salary);
console.log(user3.salary);
