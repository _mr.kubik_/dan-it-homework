export default class Card {
  constructor(usersLink, postsLink) {
    this.usersLink = usersLink;
    this.postsLink = postsLink;
  }

  render() {
    const post = this.fetchMethod(this.usersLink, this.postsLink, "GET");

    const load = document.getElementById("load");
    const row = document.getElementById("row");
    const tweet = document.getElementById("tweet");
    tweet.classList.add("disabled");

    setTimeout(() => {
      load.style.display = "none";
      row.style.display = "block";
      tweet.classList.remove("disabled");
    }, 3000);
  }

  async fetchMethod(usersLink, postsLink = "", connectMethod) {
    if (typeof postsLink === "object") {
      const responseUser = await fetch(usersLink, { method: connectMethod });
      const user = await responseUser.json();

      const createPost = await user.forEach(({ id, name, username }) => {
        let arr = [postsLink];
        arr.forEach(({ id: postId, userId, title, body }) => {
          if (id === userId) {
            const num = postId;
            const row = document.getElementById("row");
            const postItem = ` <div id="post${postId}" class="col bg-custom rounded m-1">
          <div class="row mx-1 me-md-5 flex-nowrap py-3">
            <img
              class="
                img-responsive
                col-3 col-md-2 col-lg-1
                rounded-circle
                align-self-start
                selector
              "
              src="/EWAJB4WUcAAje8s.png"
              alt=""
            />
            <div class="row align-items-start">
              <div class="col-12 p-2">

              <strong class="col-6"><u>${name}</u></strong> <span>@${username}</span> <button id="delete${num}" class="btn btn-primary btn-sm"><i class="bi bi-x-lg disabled "></i>
                </button> <button id="delete${num}" class="btn btn-primary btn-sm"><i class="bi bi-pencil-square disabled "></i>
                </button>
                <br />
                 ${title}
                </div>
                <br />
                <span
                  >${body}</span
                >
              </div>
            </div>
          </div>
        </div>  `;
            row.insertAdjacentHTML("afterbegin", postItem);
            //
            const deletePost = document.getElementById(`delete${num}`);
            deletePost.addEventListener("click", (e) => {
              let nums = e.target.id.replace("delete", "");
              let postId = "#post" + nums;
              let post = e.target.closest(postId);
              fetch(this.postsLink + nums, { method: "delete" }).then(
                (response) => {
                  console.log(response);
                }
              );
              post.remove();
            });

            //
          }
        });
      });
    } else if (postsLink.startsWith("https")) {
      const responseUser = await fetch(usersLink, { method: connectMethod });
      const responsePost = await fetch(postsLink, { method: connectMethod });

      const user = await responseUser.json();
      const post = await responsePost.json();

      const createPost = await user.forEach(({ id, name, username }) => {
        post.forEach(({ id: postId, userId, title, body }) => {
          if (id === userId) {
            const num = postId;
            const row = document.getElementById("row");
            const postItem = ` <div id="post${postId}" class="col bg-custom rounded m-1">
          <div class="row mx-1 me-md-5 flex-nowrap py-3">
            <img
              class="
                img-responsive
                col-3 col-md-2 col-lg-1
                rounded-circle
                align-self-start
                selector
              "
              src="/EWAJB4WUcAAje8s.png"
              alt=""
            />
            <div class="row align-items-start">
              <div class="col-12 p-2">
                <strong class="col-6"><u>${name}</u></strong> <span>@${username}</span> <button id="delete${num}" class="btn btn-primary btn-sm"><i class="bi bi-x-lg disabled "></i>
                </button>
                <br />
                 ${title}
                </span>
                <br />
                <span
                  >${body}</span
                >
              </div>
            </div>
          </div>
        </div>  `;
            row.insertAdjacentHTML("afterbegin", postItem);
            //
            const deletePost = document.getElementById(`delete${num}`);
            deletePost.addEventListener("click", (e) => {
              let nums = e.target.id.replace("delete", "");
              let postId = "#post" + nums;
              let post = e.target.closest(postId);
              fetch(this.postsLink + nums, { method: "delete" }).then(
                (response) => {
                  console.log(response);
                }
              );
              post.remove();
            });

            //
          }
        });
      });
    }
  }

  newPost(object) {
    let newObj = object;
    fetch(this.postsLink, {
      method: "POST",
      body: JSON.stringify(object),
      headers: {
        "Content-Type": "application/json",
      },
    }).then((response) => this.fetchMethod(this.usersLink, newObj, "GET"));
  }

  // publishPost(object) {
  //   let { id, userId, title, body } = object;
  //   const num = postId;
  //   const row = document.getElementById("row");
  //   const postItem = ` <div id="post${postId}" class="col bg-custom rounded m-1">
  //         <div class="row mx-1 me-md-5 flex-nowrap py-3">
  //           <img
  //             class="
  //               img-responsive
  //               col-3 col-md-2 col-lg-1
  //               rounded-circle
  //               align-self-start
  //               selector
  //             "
  //             src="/EWAJB4WUcAAje8s.png"
  //             alt=""
  //           />
  //           <div class="row align-items-start">
  //             <div class="col-12 p-2">
  //               <strong class="col-6"><u>Nazarii</u></strong> <span>@Nazarii</span> <button id="delete${num}" class="btn btn-primary btn-sm">X</button>
  //               <br />
  //                ${title}
  //               </span>
  //               <br />
  //               <span
  //                 >${body}</span
  //               >
  //             </div>
  //           </div>
  //         </div>
  //       </div>  `;
  //   row.insertAdjacentHTML("afterbegin", postItem);
  // }
}
