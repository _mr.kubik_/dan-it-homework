import Card from "./Card.js";

const users = "https://ajax.test-danit.com/api/json/users/";
const posts = "https://ajax.test-danit.com/api/json/posts/";

const card1 = new Card(users, posts);

card1.render();

const form = document.getElementById(`form`);

form.addEventListener("submit", (e) => {
  e.preventDefault();
  let title = document.getElementById("postTitle").value;
  let text = document.getElementById("postText").value;

  const object = {
    id: 1,
    userId: 1,
    title: title,
    body: text,
  };

  card1.newPost(object);

  form.reset();
});
