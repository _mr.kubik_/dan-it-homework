import "./App.css";
import React from "react";
import Button from "./components/Button";

class App extends React.Component {
  render() {
    return (
      <>
        <Button
          text="Open first modal"
          backgroundColor={"#CDDFA0"}
          width={"300px"}
          height={"200px"}
          modalTitle={"Do you want to delete this file?"}
          modalText={
            "Once you delete this file, it won't be possible to undo this action. Are you sure you want to delete it?"
          }
          headerModal={"#d44637"}
          bodyModal={"#e74c3c"}
          buttonsModal={"#b3382c"}
          closeButton={false}
          firstButton={"Ok"}
          secondButton={"Cancel"}
        />
        <Button
          text="Open second modal"
          backgroundColor={"#38405F"}
          width={"300px"}
          height={"200px"}
          modalTitle={"Do you want to save this file?"}
          modalText={
            "By saving the file you will be able to access it on our cloud server"
          }
          headerModal={"#9C7A97"}
          bodyModal={"#888DA7"}
          buttonsModal={"#303633"}
          closeButton={true}
          firstButton={"Save"}
        />
      </>
    );
  }
}

export default App;
