import React from "react";
import Button from "../Button/Button";
import FlexElement from "../Reusable/FlexElement";
import "./Item.scss";
import Star from "./Star";

class ItemCard extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      showDesc: false,
      id: this.props.id,
      name: this.props.name,
      itemCode: this.props.itemCode,
      price: this.props.price,
      color: this.props.color,
      url: this.props.url,
      starColor: this.props.starColor,
      object: null,
    };
  }

  showInfo = () => {
    if (!this.state.showDesc) {
      return this.setState({ showDesc: true });
    } else {
      return this.setState({ showDesc: false });
    }
  };

  passObject = (e) => {
    const requestOptions = {
      method: "PUT",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({
        color: this.state.color,
        id: this.state.id,
        name: this.state.name,
        price: this.state.price,
        itemCode: this.state.itemCode,
        key: this.state.time,
        url: this.state.url,
        starColor: true,
        favColor: "#ffffff",
      }),
    };

    fetch("http://localhost:8000/itemsCards/" + this.state.id, requestOptions)
      .then((res) => res.json())
      .then((data) => console.log(data));

    this.props.favoriteFunc(this.state.id, "fav");
  };

  componentDidMount() {
    if (this.state.starColor) {
      this.setState({ favColor: "#ffcd2b" });
    }
    if (!this.state.starColor) {
      this.setState({ favColor: "#ffffff" });
    }
  }

  cartAdd = (e) => {
    const obj = this.state.id;
    this.props.favoriteFunc(obj, "cart");
  };

  render() {
    const border = {
      borderTop: `13px solid ${this.state.color}`,
    };

    const inner = {
      background: this.state.color,
    };
    return (
      <>
        <div className="itemCard">
          <div className="itemCard__wrapper">
            <h1 className="itemCard__title">{this.state.name}</h1>
            <button
              onClick={() => {
                this.passObject();
              }}
              className="itemCard__favorite"
            >
              <Star fill={this.state.favColor} class={"favoriteStar"} />
            </button>
          </div>

          <img
            className="itemCard__cookieImg"
            src={this.state.url}
            style={border}
            alt=""
          />
          <div onClick={this.showInfo} className="itemCard__showBtn">
            Show more...
          </div>
          {this.state.showDesc ? (
            <div className="itemCard__info">
              <p className="itemCard__price" style={inner}>
                price {this.state.price} pack
              </p>
              <Button
                btnTitle="Open first modal"
                backgroundColor={`${this.state.color}`}
                textColor={"#FFFFFF"}
                width={"160px"}
                height={"35px"}
                btnName="openModal"
                onClick={this.props.rateFunc(
                  true,
                  "Do you want to add these cookies to the cart?",
                  "Tastes good 🍪",
                  "#197278",
                  "#C5C9A4",
                  true,
                  [
                    <Button
                      textColor={"#FFFFFF"}
                      key="3"
                      backgroundColor={"#197278"}
                      width={"101px"}
                      height={"41px"}
                      btnName="closeModal"
                      onClick={() => {
                        this.props.closeFunc();
                        this.cartAdd();
                      }}
                    >
                      Add
                    </Button>,
                  ]
                )}
              >
                Add to cart
              </Button>
              <span className="itemCard__code" style={inner}>
                {this.state.itemCode}
              </span>
            </div>
          ) : null}
        </div>
      </>
    );
  }
}

export default ItemCard;
