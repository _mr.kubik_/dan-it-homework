import React from "react";
import Button from "../Button/Button";
import FlexElement from "../Reusable/FlexElement";
import "./Header.scss";

class Header extends React.Component {
  render() {
    return (
      <div className="header">
        <FlexElement justify={"space-between"} color={"#ffffff"} radius={"0px"}>
          <Button
            textColor={"#FFFFFF"}
            key="2"
            backgroundColor={"#b3382c"}
            width={"41px"}
            height={"41px"}
            onClick={this.props.switcherState}
            btnName="cart"
          >
            <img src="images/cart.svg" alt="" />
          </Button>
          <Button
            textColor={"#FFFFFF"}
            key="2"
            backgroundColor={"#b3382c"}
            width={"41px"}
            height={"41px"}
            onClick={this.props.switcherState}
            btnName="main"
          >
            <img className="icon" src="images/cookie-main.svg" alt="" />
          </Button>
          <Button
            textColor={"#FFFFFF"}
            key="2"
            backgroundColor={"#b3382c"}
            width={"41px"}
            height={"41px"}
            onClick={this.props.switcherState}
            btnName="favorite"
          >
            <img src="images/favorite.svg" alt="" />
          </Button>
        </FlexElement>
      </div>
    );
  }
}

export default Header;
