import React from "react";

import "./Button.style.scss";

class Button extends React.PureComponent {
  render() {
    const btn = {
      backgroundColor: this.props.backgroundColor,
      width: this.props.width,
      height: this.props.height,
      display: this.props.closeButton,
      color: this.props.textColor,
    };
    return (
      <>
        <button
          onClick={this.props.onClick}
          className={`btn ${this.props.btnName}`}
          style={btn}
        >
          {this.props.children}
        </button>
      </>
    );
  }
}

export default Button;
