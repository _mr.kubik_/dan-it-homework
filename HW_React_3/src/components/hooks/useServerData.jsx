import { useEffect, useState } from "react";

const useServerData = (param) => {
  const [favArray, setFavArray] = useState(null);
  const [cartArray, setCartArray] = useState(null);
  const [cookies, setCookies] = useState([]);

  useEffect(() => {
    fetch("http://localhost:8000/itemsCards")
      .then((res) => res.json())
      .then((data) => setCookies(data));

    if (param === "cart") {
      const cart = JSON.parse(localStorage.getItem("cartArray"));

      setCartArray(cart);
    } else if (param === "favorite") {
      const fav = JSON.parse(localStorage.getItem("favArray"));

      setFavArray(fav);
    }
  }, []);
  if (param === "cart") {
    return [cookies, cartArray];
  } else if (param === "favorite") {
    return [cookies, favArray];
  }
};

export default useServerData;
