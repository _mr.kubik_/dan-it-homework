import { Field, Formik, Form, ErrorMessage } from "formik";
import { useSelector, useDispatch } from "react-redux";
import Button from "../Button/Button";
import "./Form.scss";
import CheckoutSchema from "./ValidationOfForm";
import React from "react";
import { requestLocalStorageCart } from "../../store/actions";
import NumberFormat from "react-number-format";

const FormModal = (props) => {
  const cartArray = useSelector((state) => state.local.cartArray);
  const items = useSelector((state) => state.store.items);

  const newItems = items.filter((item) => cartArray.includes(item.id));

  const dispatch = useDispatch();

  const handleSubmit = () => {
    newItems.map((item) => {
      for (const [key, value] of Object.entries(item)) {
        console.log(`${key}: ${value}`);
      }
      console.log("-------------------");
      dispatch(requestLocalStorageCart([]));
      localStorage.removeItem("cartArray");
      return true;
    });
  };

  return (
    <Formik
      initialValues={{
        firstName: "",
        lastname: "",
        age: "",
        address: "",
        mobileNumber: "",
      }}
      onSubmit={(values) => {
        for (const [key, value] of Object.entries(values)) {
          console.log(`${key}: ${value}`);
        }
        console.log("-----------------------------");
        handleSubmit();
        props.closeFunc();
      }}
      validationSchema={CheckoutSchema}
    >
      {(formik) => (
        <Form className="form">
          <div className="form-group">
            <label htmlFor="firstName">First Name:</label>

            <Field
              className="form-group__input"
              name="firstName"
              id="firstName"
            />
            <ErrorMessage
              name="firstName"
              className={"error"}
              component={"div"}
            />
          </div>
          <div className="form-group">
            <label htmlFor="lastname">Last Name:</label>

            <Field
              className="form-group__input"
              name="lastname"
              id="lastname"
            />
            <ErrorMessage
              name="lastname"
              className={"error"}
              component={"div"}
            />
          </div>
          <div className="form-group">
            <label htmlFor="age">Age:</label>

            <Field
              type="number"
              className="form-group__input"
              name="age"
              id="age"
              min="1"
            />

            <ErrorMessage name="age" className={"error"} component={"div"} />
          </div>
          <div className="form-group">
            <label htmlFor="address">Address:</label>

            <Field className="form-group__input" name="address" id="address" />
            <ErrorMessage
              name="address"
              className={"error"}
              component={"div"}
            />
          </div>
          <div className="form-group">
            <label htmlFor="mobileNumber">Mobile Number:</label>

            <NumberFormat
              type="text"
              name="mobileNumber"
              id="mobileNumber"
              className="form-group__input"
              format="(###) ###-####"
              mask="_"
              value={formik.values.mobileNumber}
              onBlur={formik.handleBlur}
              onValueChange={(values) => {
                const { value } = values;
                formik.setFieldValue("mobileNumber", value);
              }}
            />

            <ErrorMessage
              name="mobileNumber"
              className={"error"}
              component={"div"}
            />
          </div>
          <div className="form-button">
            <Button
              textColor={"#FFFFFF"}
              key="3"
              backgroundColor={"#5f944f"}
              width={"101px"}
              height={"41px"}
              btnName="closeModal"
              type="submit"
            >
              Checkout
            </Button>
          </div>
        </Form>
      )}
    </Formik>
  );
};

export default FormModal;
