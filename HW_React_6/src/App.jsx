import "./App.scss";
import React from "react";
import Modal from "./components/Modal/Modal";
import Main from "./components/Body/Store/Main";
import Header from "./components/Header/Header.jsx";
import { Routes, Route } from "react-router-dom";
import Cart from "./components/Body/Cart/Cart";
import Favorite from "./components/Body/Favorite/Favorite";
import { useDispatch, useSelector } from "react-redux";
import { showModal } from "./store/actions";
import Button from "./components/Button/Button";

const App = () => {
  const dispatch = useDispatch();

  const closeModal = (e) => {
    dispatch(showModal(false));
  };

  const showM = useSelector((store) => store.modal.showModal);

  const modalInner = useSelector((store) => store.modal.modalInner);

  const mock = {
    header: "",
    headerModal: "#783419",
    closeButton: true,
    bodyModal: "#c9b0a4",
    actions: [
      <Button
        textColor={"#FFFFFF"}
        key="3"
        backgroundColor={"#197278"}
        width={"101px"}
        height={"41px"}
        btnName="closeModal"
      >
        Add
      </Button>,
    ],
    modalText: "❗",
  };

  return (
    <>
      <Routes>
        <Route path={"/"} element={<Header />}>
          <Route path={"cart"} element={<Cart closeFunc={closeModal} />} />
          <Route index element={<Main closeFunc={closeModal} />} />
          <Route
            path={"favorite"}
            element={<Favorite closeFunc={closeModal} />}
          />
        </Route>
      </Routes>

      <Modal showM={showM} modalInner={modalInner} closeModal={closeModal} />
    </>
  );
};

export default App;
