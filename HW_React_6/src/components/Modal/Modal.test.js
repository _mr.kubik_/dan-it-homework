import React from "react";
import { render, unmountComponentAtNode } from "react-dom";
import { act } from "react-dom/test-utils";
import Button from "../Button/Button";
import Modal from "./Modal";

let container = null;

beforeEach(() => {
  container = document.createElement("div");
  document.body.append(container);
});

afterEach(() => {
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

describe("renders with inner context or without", () => {
  it("renders with inner context", async () => {
    let promise = Promise.resolve();

    render(<Modal />, container);
    expect(container.textContent).toContain("");

    const mock = {
      header: "Do you want to remove these cookies from the cart?",
      headerModal: "#783419",
      closeButton: true,
      bodyModal: "#c9b0a4",
      actions: [],
      modalText: "❗",
    };

    const result = mock.header + mock.modalText + mock.actions;

    render(<Modal showM={true} modalInner={mock} />, container);
    expect(container.textContent).toBe(result);

    await act(() => promise);
  });

  it("renders without inner context", async () => {
    let promise = Promise.resolve();

    render(<Modal />, container);
    expect(container.textContent).toContain("");

    const mock = {
      header: "",
      headerModal: "#783419",
      closeButton: true,
      bodyModal: "#c9b0a4",
      actions: [
        <Button
          textColor={"#FFFFFF"}
          key="3"
          backgroundColor={"#197278"}
          width={"101px"}
          height={"41px"}
          btnName="closeModal"
        >
          Add
        </Button>,
      ],
      modalText: "❗",
    };

    const result =
      mock.header +
      mock.modalText +
      mock.actions.map((element) => element.props.children);

    render(<Modal showM={true} modalInner={mock} />, container);
    expect(container.textContent).toBe(result);

    await act(() => promise);
  });
});
