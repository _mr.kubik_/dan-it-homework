"use strict";
// Flag
let flag = true;

while (flag == true) {
  // Name filter
  let name = prompt("What is your name?");

  while (name === "" || name == null || !isNaN(+name)) {
    alert("This is not a name");
    name = prompt("What is your name?");
    if (name === "" || name == null || !isNaN(+name)) {
      continue;
    } else {
      break;
    }
  }

  // Age filter

  let age = +prompt("What is your age?");

  while (isNaN(age) || typeof +age != "number" || !age) {
    alert("This is not a number");
    age = +prompt("What is your age?");
    if (isNaN(age) || typeof +age != "number" || !age) {
      continue;
    } else {
      break;
    }
  }

  // Age filter
  if (age < 18) {
    alert("You are not allowed to visit this website");
    break;
  } else if (18 <= age && age <= 22) {
    let conf = confirm("Are you sure you want to continue?");
    if (conf === true) {
      alert(`Welcome ${name}`);
      break;
    } else {
      alert("You are not allowed to visit this website");
      break;
    }
  } else {
    alert(`Welcome ${name}`);
  }
}
