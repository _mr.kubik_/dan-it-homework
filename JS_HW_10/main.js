"use strict";

const form = document.getElementsByClassName("password-form")[0];

const input = document.getElementsByClassName("password");

const alertNotification = document.getElementById("match");

alertNotification.style.color = "red";

form.addEventListener("click", (e) => {
  let target = e.target;

  if (target.classList.contains("fa-eye")) {
    target.classList.replace("fa-eye", "fa-eye-slash");
    if (target.previousElementSibling.tagName === "INPUT") {
      target.previousElementSibling.setAttribute("type", "text");
    }
  } else if (target.classList.contains("fa-eye-slash")) {
    target.classList.replace("fa-eye-slash", "fa-eye");
    if (target.previousElementSibling.tagName === "INPUT") {
      target.previousElementSibling.setAttribute("type", "password");
    }
  }
});

form.addEventListener("submit", (e) => {
  e.preventDefault();
  if (input[0].value === input[1].value) {
    alertNotification.textContent = "";
    alert("Welcome!");
  } else {
    alertNotification.textContent = "Нужно ввести одинаковые значения";
  }
});

function switcher(target) {
  if (target.classList.contains("fa-eye")) {
    target.classList.replace("fa-eye", "fa-eye-slash");
  } else if (target.classList.contains("fa-eye-slash")) {
    target.classList.replace("fa-eye-slash", "fa-eye");
  }
}
