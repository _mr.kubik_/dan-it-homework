const button = document.getElementById("green-button");

window.addEventListener("load", () => {
  if (localStorage.getItem("mainColor")) {
    let color = localStorage.getItem("mainColor");
    document.documentElement.style.setProperty("--main", `${color}`);
  } else {
    localStorage.setItem("mainColor", "#E96021");
    let color = localStorage.getItem("mainColor");
    document.documentElement.style.setProperty("--main", `${color}`);
  }
});

button.addEventListener("click", (event) => {
  if (window.getComputedStyle(button).backgroundColor === "rgb(233, 96, 33)") {
    localStorage.setItem("mainColor", "#258F25");
    button.textContent = "Green Millan";
    let color = localStorage.getItem("mainColor");
    document.documentElement.style.setProperty("--main", `${color}`);
  } else if (
    window.getComputedStyle(button).backgroundColor === "rgb(37, 143, 37)"
  ) {
    localStorage.setItem("mainColor", "#E96021");
    button.textContent = "Orange Millan";
    let color = localStorage.getItem("mainColor");
    document.documentElement.style.setProperty("--main", `${color}`);
  }
});
