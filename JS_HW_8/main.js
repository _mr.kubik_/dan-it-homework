"use strict";

const input = document.createElement("input");
input.placeholder = "Price";

const div1 = document.createElement("div");
const div2 = document.createElement("div");

const span = document.createElement("span");
const button = document.createElement("button");
button.textContent = "\u2716";

document.body.append(div1);
document.body.append(div2);

div2.append(input);

input.addEventListener("focusin", function () {
  console.log("Focused");
});

function blur() {
  if (input.value < 0 || isNaN(+input.value)) {
    input.style.cssText = "outline: solid red";
    span.textContent = `Please enter correct price`;
    span.style.color = "red";
    button.style.display = "none";

    document.body.append(span);
  } else if (input.value === "") {
    span.style.color = "black";
  } else {
    input.style.cssText = "outline: solid green";
    span.textContent = `Текущая цена: ${input.value}`;
    span.style.color = "black";

    div1.append(span);
    button.style.display = "inline-block";
    div1.append(button);
  }
}

input.addEventListener("blur", blur);

button.addEventListener("click", function (e) {
  button.remove();
  span.remove();
  input.style.cssText = "outline: initial";

  input.value = "";
});
