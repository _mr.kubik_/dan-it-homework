"use strict";
let active;

const tabs = document.getElementById("tabs");

const li = [...document.getElementsByClassName("list-items")];

li.forEach((element) => {
  element.style.display = "none";
});

tabs.addEventListener("click", (e) => {
  let target = e.target;

  li.forEach((element) => {
    if (element.textContent.includes(target.textContent)) {
      if ((element.style.display = "none")) {
        highlight(target);

        element.style.display = "block";
      }
    } else if (!element.textContent.includes(target.textContent)) {
      element.style.display = "none";
    }
  });
});

function highlight(element) {
  if (!active) {
    active = element;
    active.classList.add("active");
  } else if (active) {
    active.classList.remove("active");
    active = null;
    active = element;
    active.classList.add("active");
  }
}
