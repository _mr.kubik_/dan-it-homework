# Dan IT Homework

#### Contents of Repository:

### HTML/CSS Homework

1. HW_1 (Complete)
2. HW_2 (Complete)
3. HW_3 (Complete)
4. HW_4 (Complete)
5. HW_5 (Complete)
6. HW_6 (Complete)
7. HW_7 (Complete)

### JavaScript Homework

1. JS_HW_1 (Complete)
2. JS_HW_2 (Complete)
3. JS_HW_3 (Complete)
4. JS_HW_4 (Complete)
5. JS_HW_5 (Complete)
6. JS_HW_6 (Complete)
7. JS_HW_7 (Complete)
8. JS_HW_8 (Complete)
9. JS_HW_9 (Complete)
10. JS_HW_10 (Complete)
11. JS_HW_11 (Complete)
12. JS_HW_12 (Complete)
13. JS_HW_13 (Complete)
14. JS_HW_14 (Complete)
15. JS_HW_15 (Complete)
16. JS_HW_16 (Complete)

### FE Homework

1. FE_HW_1 (Complete)
2. FE_HW_2 (Complete)

### FE_JS Homework

1. FE_JS_HW_1 (Complete)
2. FE_JS_HW_2 (Complete)
3. FE_JS_HW_3 (Complete)
4. FE_JS_HW_4 (Complete)
5. FE_JS_HW_5 (Complete)
6. FE_JS_HW_6 (Complete)

### React Homework

1. HW_React_1 (Complete)
2. HW_React_2 (Incomplete)
3. HW_React_3 (Incomplete)
4. HW_React_4 (Incomplete)
5. HW_React_5 (Incomplete)
6. HW_React_6 (Incomplete)
